package fr.erdprt.spring.cloud.api.products.model;

public class Product {

    private String name;

    private Integer id;
    
    public Product(final Integer id, final String name) {
      this.name = name;
      this.id  = id;
    }
    
    public String getName() {
      return this.name;
    }
    
    public void setName(final String name) {
      this.name = name;
    }
    
    public Integer getId() {
      return this.id;
    }
    
    public void setId(final Integer id) {
      this.id = id;
    }

}